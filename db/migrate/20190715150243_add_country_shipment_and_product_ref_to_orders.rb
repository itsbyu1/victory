class AddCountryShipmentAndProductRefToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :country, :string
    add_column :orders, :shipment_ref, :string
    add_column :orders, :product_ref, :string
  end
end
