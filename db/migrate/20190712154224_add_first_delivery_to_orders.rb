class AddFirstDeliveryToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :first_delivery, :date
  end
end
