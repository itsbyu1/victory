class UpdateFieldTypesInOrders < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :shipping_address, 'json USING CAST(shipping_address AS json)'
    change_column :orders, :note_attributes, 'json USING CAST(note_attributes AS json)'
    change_column :orders, :line_items, 'json USING CAST(line_items AS json)'
    change_column :orders, :tags, 'json USING CAST(tags AS json)'
  end
end
