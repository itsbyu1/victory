class AddNotesToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :order_notes, :string
  end
end
