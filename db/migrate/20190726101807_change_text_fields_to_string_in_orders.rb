class ChangeTextFieldsToStringInOrders < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :address1, :string
    change_column :orders, :address2, :string
    change_column :orders, :city, :string
    change_column :orders, :province_code, :string
    change_column :orders, :zip, :string
    change_column :orders, :company, :string
    change_column :orders, :phone, :string
  end
end
