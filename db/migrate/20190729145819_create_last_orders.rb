class CreateLastOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :last_orders do |t|
      t.timestamps
      t.string :created_at
      t.string :last_order_id
      t.string :last_order_name
      t.string :shipment_date
      t.string :tracking_number
      t.string :tracking_url
    end
  end
end
