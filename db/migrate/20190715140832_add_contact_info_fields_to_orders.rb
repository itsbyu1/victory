class AddContactInfoFieldsToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :company, :text
    add_column :orders, :phone, :text
  end
end
