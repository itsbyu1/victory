class ReorderOrdersTable < ActiveRecord::Migration[5.2]
  def up
    change_table :orders do |o|
      o.change :order_number, :string, after: :order_id
      o.change :customer, :string, after: :order_number
      o.change :company, :text, after: :customer
      o.change :shipping_address, :json, after: :company
      o.change :address1, :text, after: :shipping_address
      o.change :address2, :text, after: :address1
      o.change :city, :text, after: :address2
      o.change :state, :text, after: :city
      o.change :zip, :text, after: :state
      o.change :phone, :text, after: :zip
      o.change :email, :string, after: :phone
      o.change :note_attributes, :json, after: :email
      o.change :first_delivery, :date, after: :note_attributes
      o.change :line_items, :json, after: :first_delivery
      o.change :tags, :json, after: :line_items
      o.change :fulfillment_status, :string, after: :tags
    end
  end
end
