class AddLastOrderToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :last_order, :string
  end
end
