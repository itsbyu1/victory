class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :order_id
      t.string :order_number
      t.date :created_at
      t.text :shipping_address
      t.string :email
      t.string :customer
      t.text :note_attributes
      t.text :line_items
      t.text :tags
      t.string :fulfillment_status
    end
  end
end
