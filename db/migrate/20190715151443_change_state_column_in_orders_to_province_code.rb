class ChangeStateColumnInOrdersToProvinceCode < ActiveRecord::Migration[5.2]
  def change
    rename_column :orders, :state, :province_code
    rename_column :orders, :country, :country_code
  end
end
