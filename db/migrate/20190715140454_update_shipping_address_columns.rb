class UpdateShippingAddressColumns < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :address1, :text
    add_column :orders, :address2, :text
    add_column :orders, :city, :text
    add_column :orders, :state, :text
    add_column :orders, :zip, :text
  end
end
