class ChangeTagsFromJsonToString < ActiveRecord::Migration[5.2]
  def change
    change_column :orders, :tags, :string
  end
end
