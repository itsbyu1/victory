require 'test_helper'

class OrderControllerTest < ActionDispatch::IntegrationTest
  test "should get export" do
    get order_export_url
    assert_response :success
  end

end
