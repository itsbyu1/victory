# frozen_string_literal: true

ShopifyApp.configure do |config|
  config.application_name = 'Victory'
  config.api_key = ENV['SHOPIFY_API_KEY']
  config.secret = ENV['SHOPIFY_SECRET']
  config.old_secret = '<old_secret>'
  config.scope = 'read_products, read_orders, write_orders, read_customers, write_customers, read_fulfillments, write_fulfillments, read_checkouts' # Consult this page for more scope options:
  # https://help.shopify.com/en/api/getting-started/authentication/oauth/scopes
  config.embedded_app = true
  config.after_authenticate_job = false
  config.session_repository = Shop
end
