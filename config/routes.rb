Rails.application.routes.draw do
  mount ShopifyApp::Engine, at: '/'
  resources :orders
  get 'create', action: :create, controller: 'orders'
  root to: 'orders#index'
  get 'export', to: 'orders#export', as: :orders_export
  get 'export_full', to: 'orders#export_full', as: :orders_export_full
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
