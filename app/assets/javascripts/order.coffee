# $(document).ready ->
#   $('#dttb').dataTable({
#     "columnDefs": [
#       {
#         "orderable": false
#         "targets": [0, 1, 2, 6, 7]
#       }
#     ]
#   })
#   return

$(document).on 'click', '.parent', ->
  child = $(this).next('.child')
  if child.hasClass('visible')
      child.removeClass('visible').hide()
  else
      child.toggleClass('visible').css('display', 'table-row')
  return
return