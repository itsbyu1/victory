class Order < ActiveRecord::Base
    has_one :last_order
    
    validates   :order_id, 
                :presence => {:message => "Order ID cannot be blank"},
                :uniqueness => {:message => "Order already exists"}
            
    paginates_per 25
    
    def self.to_csv(attributes = column_names, options = {})

        CSV.generate(options) do |csv|
            csv.add_row attributes

            # Iterate through all the rows.
            all.each do |foo|

                values = foo.attributes.slice(*attributes).values

                csv.add_row values
            end
        end
    end
end