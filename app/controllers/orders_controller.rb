class OrdersController < AuthenticatedController
  require 'csv'
  helper_method :sort_column, :sort_direction

  def index
    @orders = Order.order(sort_column + " " + sort_direction).page params[:page]
    @shop = ShopifyAPI::Shop.current.domain
  end

  def create

    # Shopify Order data
    Order.delete_all
    @orders = ShopifyAPI::Order.find(:all, params: [fulfillment_status: :unshipped, fulfillment_status: :partial, limit: 250])
    @orders = @orders.as_json

    @orders.each do |o|
      order = Order.new
      order.created_at = o["created_at"]
      order.order_id = o["id"]
      order.order_number = "1-" + o["order_number"].to_s
      order.customer = o["shipping_address"].name
      order.company = o["shipping_address"].company
      order.address1 = o["shipping_address"].address1
      order.address2 = o["shipping_address"].address2
      order.city = o["shipping_address"].city
      order.province_code = o["shipping_address"].province_code
      order.zip = o["shipping_address"].zip
      order.phone = o["shipping_address"].phone
      order.country_code = o["shipping_address"].country_code
      order.email = o["email"]
      order.order_notes = o["note"]
      order.last_order = o["customer"].last_order_id.to_s

      # Pack all skus into product_ref array
      order_skus = []
      o["line_items"].each do |item|
        if item.sku
          order_skus << item.sku
        end
      end
      order.product_ref = order_skus.join(', ')

      # Calculate the user-chosen first delivery date
      notes = []
      order.note_attributes = o["note_attributes"]
      order.note_attributes.each do |note|
        notes << note["name"] + ": " + note["value"]
        if note["name"] == 'First Delivery'
          first_delivery = note["value"]
          order.first_delivery = first_delivery
        end

        # Use preferred day as shipment ref for fulfilment team
        if note["name"] == 'Preferred Delivery Day'
          preferred_day = note["value"]
          order.shipment_ref = preferred_day
        else 
          order.shipment_ref = "Thursday"
        end
      end

      order.note_attributes = notes

      line_items = []
      order.line_items = o["line_items"]
      order.line_items.each do |item|
        if item["variant_title"] && item["variant_title"].length > 1
          title = item["variant_title"]
        else 
          title = item["title"]
        end
        if item["sku"] && item["sku"].length > 1
          title = title + " | " + item["sku"].to_s
        end
        line_items << title + ": " + item["quantity"].to_s
      end
      order.line_items = line_items

      order.tags = o["tags"]
      order.fulfillment_status = if o["fulfillment_status"].nil?
        'Unfulfilled'
      else
        o["fulfillment_status"].capitalize
      end

      order.save
    end
  end

  def export
    @data = Order.order(:created_at)
    attributes_to_include = %w(order_number customer company address1 address2 city province_code zip phone country_code email shipment_ref product_ref)
    respond_to do |format|
      format.html
      format.csv { send_data @data.to_csv(attributes_to_include), filename: "orders-#{Date.today}.csv" }
    end
  end

  def export_full
    @data = Order.order(:created_at)
    respond_to do |format|
      format.html
      format.csv { send_data @data.to_csv(), filename: "orders-#{Date.today}.csv" }
    end
  end

  private

  def sort_column 
    Order.column_names.include?(params[:sort]) ? params[:sort] : "customer"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
